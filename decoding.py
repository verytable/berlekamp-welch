__author__ = 'arseny'

import sys


def extended_gcd(a, b):
    """Finds greatest common divisor of two integers and its decomposition
    d = a * x1 + b * x2

    Return value: (int, int, int)

    """

    if a == 0:
        return b, 0, 1
    (d, x1, y1) = extended_gcd(b % a, a)
    return d, y1 - (b // a) * x1, x1


def inverse(a, q):
    """Finds element b such that a * b = 1 mod(q)

    Keyword arguments:r
    int: a -- residue
    int: q -- modulus

    Return value: int

    """

    (g, x, y) = extended_gcd(a, q)
    if g != 1:
        return 0
    else:
        return (x % q + q) % q


def gauss(a, q):
    """ Solves system of linear equations in modulus

    Keyword arguments:
    a -- matrix of SLE with column of free terms
    q -- modulus

    Return value: (int: solutions number,
                   list(list(int)): one of solutions (if any))

    """

    n = len(a)
    m = len(a[0]) - 1
    inf = 1e17

    where = [-1] * m
    col = 0
    row = 0
    while col < m and row < n:
        sel = row
        for i in range(row, n):
            if a[i][col] != 0:
                sel = i
                break
        if a[sel][col] % q == 0:
            continue
        for i in range(col, m + 1):
            a[sel][i], a[row][i] = a[row][i], a[sel][i]
        where[col] = row

        for i in range(n):
            if i != row:
                c = (a[i][col] * inverse(a[row][col], q)) % q
                for j in range(col, m + 1):
                    a[i][j] = ((a[i][j] - a[row][j] * c) % q + q) % q
        row += 1
        col += 1

    ans = [0] * m
    for i in range(m):
        if where[i] != -1:
            ans[i] = (a[where[i]][m] * inverse(a[where[i]][i], q)) % q
    for i in range(n):
        s = 0
        for j in range(m):
            s = (s + ans[j] * a[i][j]) % q
        if abs(s - a[i][m]) % q != 0:
            return 0, [[]]

    for i in range(m):
        if where[i] == -1:
            return inf, ans
    return 1, ans


def build_matrix(s, w, n, k, q):
    """ Constructs matrix of system of linear equations

    Keyword arguments:
    s -- deg(E)
    w -- word to decode
    n -- length of words in code
    k -- number of words in code
    q -- modulus

    Return value: list(list(int))

    """

    m = [[0 for col in range(2 * s + k + 1)] for row in range(n)]
    for i in range(n):
        for j in range(s):
            m[i][j] = ((-w[i] * i ** j) % q + q) % q
        for j in range(s + k):
            m[i][s + j] = (i ** j) % q
        m[i][2 * s + k] = (w[i] * i ** s) % q
    return m


def degree(poly, q):
    """Normalizes polynomial poly

     Return value: int -- size of poly

    """

    while poly and poly[-1] % q == 0:
        poly.pop()
    return len(poly) - 1


def poly_div(poly_a, poly_b, q):
    """Finds quotient and remainder of division a on b modulus q

    Return value: (list(int), list(int))

    """

    deg_a = degree(poly_a, q)
    deg_b = degree(poly_b, q)
    if deg_b < 0:
        raise ZeroDivisionError
    if deg_a >= deg_b:
        quotient = [0] * deg_a
        while deg_a >= deg_b:
            d = [0] * (deg_a - deg_b) + poly_b
            m = quotient[deg_a - deg_b] = \
                ((poly_a[-1] * inverse(d[-1], q)) % q + q) % q
            d = [(((c * m) % q) + q) % q for c in d]
            poly_a = [((ca - cd) % q + q) % q for ca, cd in zip(poly_a, d)]
            deg_a = degree(poly_a, q)
        remainder = poly_a
    else:
        quotient = [0]
        remainder = poly_a
    return quotient, remainder


def poly_value(poly, x, q):
    """Computes poly(x) mod q

    Return value: int

    """

    val = 0
    for i in range(len(poly)):
        val = ((val + poly[i] * x ** i) % q + q) % q
    return val


def decode(q, n, k, w):
    """Finds decoded word v such that d(w, v) <= (n - k) / 2

    Keyword arguments:
    q -- modulus
    n -- length of words in code
    k -- number of words in code
    w -- word to decode

    Return value: list(char)

    """

    for s in range(0, 1 + int((n - k) / 2)):
        m = build_matrix(s, w, n, k, q)
        coefficients = gauss(m, q)
        if coefficients[0] != 0:
            break
    poly_e = coefficients[1][:s]
    poly_e.append(1)
    poly_u = coefficients[1][s:]
    if len(poly_e) == 1:
        poly_p = poly_u
    else:
        poly_p = poly_div(poly_u, poly_e, q)[0]
    return " ".join(map(str, [poly_value(poly_p, i, q) for i in range(n)]))


if __name__ == '__main__':
    sys.stdin = open('input.txt', 'r')
    [q, n, k] = list(map(int, input().split()))
    w = list(map(int, input().split()))
    sys.stdout = open('output.txt', 'w')
    print(decode(q, n, k, w))